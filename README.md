## Trivia game
Trivia website written in Vue. 
Allows the user to answer trivia questions in various categories, and keep a personal highscore along with their username.

## IMPORTANT NOTE ON BUILDS
Possibly only the branch "stable-build-no-css" works, please use that branch if main does not display the start page of the trivia website.

## How to use
 Can  be run locally by navigating to root folder in console and typing "npm run dev".

## Dependencies
Vue x
Vue router

